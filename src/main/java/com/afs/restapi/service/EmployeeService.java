package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }


    public List<EmployeeResponse> findAll() {
        List<Employee> employees = jpaEmployeeRepository.findAll();
        return employees.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public EmployeeResponse findById(Long id) {
        return EmployeeMapper.toResponse(getEmployeeEntityById(id));
    }

    private Employee getEmployeeEntityById(Long id) {
        return jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, EmployeeRequest employeeRequest) {
        Employee toBeUpdatedEmployee = getEmployeeEntityById(id);
        Employee newEmployee = EmployeeMapper.toEntity(employeeRequest);
        if (newEmployee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(newEmployee.getSalary());
        }
        if (newEmployee.getAge() != null) {
            toBeUpdatedEmployee.setAge(newEmployee.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        List<Employee> employeesByGender = jpaEmployeeRepository.findByGender(gender);
        return employeesByGender.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee saveEmployee = jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(saveEmployee);
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {
        return jpaEmployeeRepository.findAll(PageRequest.of(page - 1, size))
                .toList()
                .stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
