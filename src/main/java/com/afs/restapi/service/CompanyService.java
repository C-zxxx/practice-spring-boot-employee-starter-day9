package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    public List<CompanyResponse> findAll() {
        return jpaCompanyRepository.findAll()
                .stream()
                .map(CompanyMapper::toResponse)
                .collect(Collectors.toList());
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        return jpaCompanyRepository
                .findAll(PageRequest.of(page - 1, size))
                .toList().stream()
                .map(CompanyMapper::toResponse)
                .collect(Collectors.toList());
    }

    public CompanyResponse findById(Long id) {
        return CompanyMapper.toResponse(getCompanyEntityById(id));
    }

    private Company getCompanyEntityById(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Company companyToBeUpdate = getCompanyEntityById(id);
        Company newCompany = CompanyMapper.toEntity(companyRequest);
        companyToBeUpdate.setName(newCompany.getName());
        jpaCompanyRepository.save(companyToBeUpdate);

    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        return CompanyMapper.toResponse(jpaCompanyRepository.save(CompanyMapper.toEntity(companyRequest)));
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        return getCompanyEntityById(id)
                .getEmployees().stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
