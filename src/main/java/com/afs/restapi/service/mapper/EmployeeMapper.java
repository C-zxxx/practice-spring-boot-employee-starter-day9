package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public static Employee toEntity(EmployeeRequest employeeRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeRequest, employee);

        return employee;
    }

    public static Employee toEntity(EmployeeResponse employeeResponse) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeResponse, employee);

        return employee;
    }

    public static EmployeeResponse toResponse(Employee saveEmployee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(saveEmployee, employeeResponse);
        return employeeResponse;
    }
}
